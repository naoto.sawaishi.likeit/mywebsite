﻿
CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;

use mywebsite;

CREATE TABLE user(
    id int AUTO_INCREMENT PRIMARY KEY,
    login_id varchar(255) UNIQUE NOT NULL,
    name varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    is_admin boolean NOT NULL DEFAULT false,
    create_date DATETIME NOT NULL,
    update_date DATETIME NOT NULL
);

CREATE TABLE thread(
    id int AUTO_INCREMENT PRIMARY KEY,
    title varchar(100) NOT NULL,
    content varchar(255),
    prefecture_id int NOT NULL,
    situation_id int NOT NULL,
    user_id int NOT NULL,
    create_date DATETIME NOT NULL,
    FOREIGN KEY(user_id) REFERENCES user(id),
    FOREIGN KEY(prefecture_id) REFERENCES prefecture(id),
    FOREIGN KEY(situation_id) REFERENCES situation(id)
);

CREATE TABLE comment(
    id int AUTO_INCREMENT PRIMARY KEY,
    thread_id int NOT NULL,
    user_id int NOT NULL,
    comment varchar(255) NOT NULL,
    create_date DATETIME NOT NULL,
    FOREIGN KEY(thread_id) REFERENCES thread(id),
    FOREIGN KEY(user_id) REFERENCES user(id)
);

CREATE TABLE prefecture(
    id int AUTO_INCREMENT PRIMARY KEY,
    name varchar(100) NOT NULL UNIQUE
);

CREATE TABLE situation(
    id int AUTO_INCREMENT PRIMARY KEY,
    name varchar(100) NOT NULL UNIQUE
);
