package Dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import model.Prefecture;

public class PrefectureDao {

  public List<Prefecture> findAll() {
    Connection conn = null;
    List<Prefecture> prefectureList = new ArrayList<Prefecture>();

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM prefecture ORDER BY id";
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);
      while (rs.next()) {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        Prefecture prefecture = new Prefecture(id, name);
        prefectureList.add(prefecture);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return prefectureList;
  }

}
