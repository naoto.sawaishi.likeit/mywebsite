package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import model.Thread;

public class ThreadDao {

  public void insert(String title, String content, String prefectureId, String situationId,
      String userId) {
    Connection conn = null;
    // int result = 0;
    try {
      conn = DBManager.getConnection();
      String sql =
          "INSERT INTO thread(title, content, prefecture_id, situation_id, user_id, create_date) VALUES (?, ?, ?, ?, ?, now())";


      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, title);
      pStmt.setString(2, content);
      pStmt.setString(3, prefectureId);
      pStmt.setString(4, situationId);
      pStmt.setString(5, userId);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  public List<Thread> findAll() {
    Connection conn = null;
    List<Thread> threadList = new ArrayList<Thread>();

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM thread ORDER BY id";
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);
      while (rs.next()) {
        int id = rs.getInt("id");
        String title = rs.getString("title");
        String content = rs.getString("content");
        int prefectureId = rs.getInt("prefecture_id");
        int situationId = rs.getInt("situation_id");
        int userId = rs.getInt("user_id");
        Timestamp createDate = rs.getTimestamp("create_date");
        Thread thread =
            new Thread(id, title, content, prefectureId, situationId, userId, createDate);
        threadList.add(thread);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return threadList;
  }



  public Thread findById(String id) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM thread WHERE id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }
      int uId = rs.getInt("id");
      String title = rs.getString("title");
      String content = rs.getString("content");
      int prefectureId = rs.getInt("prefecture_id");
      int situationId = rs.getInt("situation_id");
      int userId = rs.getInt("user_id");
      Timestamp createDate = rs.getTimestamp("create_date");
      return new Thread(uId, title, content, prefectureId, situationId, userId, createDate);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public void delete(String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM thread WHERE id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // userを削除したときにスレッドも含めて消す用
  public void deleteByUserId(String userId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM thread WHERE user_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userId);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }


  public List<Thread> search(String title, String prefectureId, String situationId,
      String startDate, String endDate) {
    Connection conn = null;
    List<Thread> threadList = new ArrayList<Thread>();
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      /* ***SQL文作成 start*** */
      StringBuilder sql = new StringBuilder("SELECT * FROM thread ");
      List<String> parameters = new ArrayList<String>();
      String x = "";

      // タイトル
      if (!(title.equals(""))) {
        if (x.equals("")) {
          x = "a";
          sql.append(" WHERE title LIKE ? ");
        } else {
          sql.append(" AND title LIKE ? ");
        }
        parameters.add("%" + title + "%");
      }
      // // キーワードが入力されていたらsqlに追加
      if (!(situationId.equals("選択してください"))) {
        if (x.equals("")) {
          x = "a";
          sql.append(" WHERE situation_id = ? ");
        } else {
          sql.append(" AND situation_id = ? ");
        }
        parameters.add(situationId);
      }
      // 都道府県が入力されていたらsqlに追加
      if (!(prefectureId.equals("選択してください"))) {
        if (x.equals("")) {
          x = "a";
          sql.append(" WHERE prefecture_id = ? ");
        } else {
          sql.append(" AND prefecture_id = ? ");
        }
        parameters.add(prefectureId);
      }
      // /* ***検索日付範囲*** */
      // // 始まりの日付が入力されていたらsqlに追加
      if (!(startDate.equals(""))) {
        if (x.equals("")) {
          x = "a";
          sql.append(" WHERE create_date >= ? ");
        } else {
          sql.append(" AND create_date >= ? ");
        }
        parameters.add(startDate);
      }
      // 終わりの日付が入力されていたらsqlに追加
      if (!(endDate.equals(""))) {
        if (x.equals("")) {
          x = "a";
          sql.append(" WHERE create_date <= ? ");
        } else {
          sql.append(" AND create_date <= ? ");
        }
        parameters.add(endDate);
      }
      /* ***SQL文作成 end*** */
      System.out.println("h");
      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql.toString());
      for (int i = 0; i < parameters.size(); i++) {
        pStmt.setString(i + 1, parameters.get(i));
      }

      ResultSet rs = pStmt.executeQuery();
      while (rs.next()) {
        int id = rs.getInt("id");
        String resultTitle = rs.getString("title");
        String resultContent = rs.getString("content");
        int resultPrefectureId = rs.getInt("prefecture_id");
        int resultSituationId = rs.getInt("situation_id");
        int resultUserId = rs.getInt("user_id");
        Timestamp resultCreateDate = rs.getTimestamp("create_date");
        Thread thread = new Thread(id, resultTitle, resultContent, resultPrefectureId,
            resultSituationId, resultUserId, resultCreateDate);

        threadList.add(thread);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return threadList;
  }
}

