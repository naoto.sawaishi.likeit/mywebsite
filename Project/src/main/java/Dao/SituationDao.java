package Dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import model.Situation;

public class SituationDao {


  public List<Situation> findAll() {
    Connection conn = null;
    List<Situation> situationList = new ArrayList<Situation>();

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM situation ORDER BY id";
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);
      while (rs.next()) {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        Situation prefecture = new Situation(id, name);
        situationList.add(prefecture);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return situationList;
  }

}
