package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import model.User;

public class UserDao {
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();
      if (!rs.next()) {
        return null;
      }
      int id = rs.getInt("id");
      String login_id = rs.getString("login_id");
      String name = rs.getString("name");
      String userPassword = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, login_id, name, userPassword, isAdmin, createDate, updateDate);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }


  public User findById(String id) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }
      int uId = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(uId, loginId, name, password, isAdmin, createDate, updateDate);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // user_idからcommentIdのnameを取る 追加 仮 コメントに名前と日付をつける
  public User findByUserId(String userId) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql =
          "SELECT * FROM user INNER JOIN comment ON user.id = comment.user_id WHERE comment.user_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }
      int uId = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(uId, loginId, name, password, isAdmin, createDate, updateDate);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



  // idからuserが存在するかどうかを調べる。
  public Boolean findId(String id) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      ResultSet rs = pStmt.executeQuery();
      if (!rs.next()) {
        return false;
      }
      return true;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



  public Boolean findByLoginId(String loginId) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE login_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return false;
      }
      return true;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



  // ユーザ詳細画面で使う 管理者だけすべてのユーザを表示
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {

      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE is_admin = false";// WHERE is_admin =false
                                                               // をつけることで管理者は表示されない
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user = new User(id, loginId, name, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return userList;
  }

  // ユーザ登録で使う
  public void insert(String loginId, String userName, String password) {
    Connection conn = null;
    // int result = 0;
    try {
      conn = DBManager.getConnection();

      String sql =
          "INSERT INTO user (login_id, name, password, create_date, update_date) VALUES (?, ?, ?, now(), now())";



      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, userName);
      pStmt.setString(3, password);
      pStmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  // 更新
  public void update(int id, String userName, String password) {
    Connection conn = null;
    // int result = 0;
    try {
      conn = DBManager.getConnection();

      String sql =
          "UPDATE user SET name = ?, password = ?, update_date = now() WHERE id = ?";



      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userName);
      pStmt.setString(2, password);
      pStmt.setInt(3, id);
      pStmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  // passwordは更新しないバージョン
  public void update(int id, String userName) {
    Connection conn = null;
    // int result = 0;
    try {
      conn = DBManager.getConnection();

      String sql =
          "UPDATE user SET name = ?, update_date = now() WHERE id = ?";



      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userName);
      pStmt.setInt(2, id);
      pStmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }


  // 削除
  public void delete(int id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM user WHERE id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
