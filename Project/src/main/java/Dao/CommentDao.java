package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import model.Comment;

public class CommentDao {

  public void insert(String threadId, int userId, String comment) {
    Connection conn = null;
    // int result = 0;
    try {
      conn = DBManager.getConnection();
      String sql =
          "INSERT INTO comment(thread_id, user_id, comment, create_date) VALUES (?, ?, ?, now())";


      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, threadId);
      pStmt.setInt(2, userId);
      pStmt.setString(3, comment);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  // comment user_id id name create_date を使う
  public List<Comment> findAll() {
    Connection conn = null;
    List<Comment> commentList = new ArrayList<Comment>();

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM comment ORDER BY id";
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);
      while (rs.next()) {
        int id = rs.getInt("id");
        int threadId = rs.getInt("thread_id");
        int userId = rs.getInt("user_id");
        String text = rs.getString("comment");
        Timestamp createDate = rs.getTimestamp("create_date");
        Comment comment = new Comment(id, threadId, userId, text, createDate);
        commentList.add(comment);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return commentList;
  }

  public Comment findById(String id) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM comment WHERE id = ? ORDER BY id";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }
      int uId = rs.getInt("id");
      int threadId = rs.getInt("thread_id");
      int userId = rs.getInt("user_id");
      String text = rs.getString("comment");
      Timestamp createDate = rs.getTimestamp("create_date");
      return new Comment(uId, threadId, userId, text, createDate);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public List<Comment> findAllByThreadId(String commentThreadId) {
    Connection conn = null;
    List<Comment> commentList = new ArrayList<Comment>();

    try {
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM comment WHERE thread_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, commentThreadId);
      ResultSet rs = pStmt.executeQuery(sql);
      while (rs.next()) {
        int id = rs.getInt("id");
        int threadId = rs.getInt("thread_id");
        int userId = rs.getInt("user_id");
        String text = rs.getString("comment");
        Timestamp createDate = rs.getTimestamp("create_date");
        Comment comment = new Comment(id, threadId, userId, text, createDate);
        commentList.add(comment);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return commentList;
  }


  public void delete(String id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM comment WHERE id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // userを削除したときにコメントも同時に削除する用
  public void deleteByUserId(String userId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "DELETE FROM comment WHERE user_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userId);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



}
