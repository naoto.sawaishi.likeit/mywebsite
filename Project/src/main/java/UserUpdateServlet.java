

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserUpdateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    // HttpSession session = request.getSession();
    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("userInfo");
    String id = request.getParameter("id");

    try {
      if (!(loginUser.equals(null))) {
        UserDao userDao = new UserDao();
        User user = userDao.findById(id);
        if (user == null) {
          request.setAttribute("errMsg", "エラーメッセージ");
          RequestDispatcher dispatcher =
              request.getRequestDispatcher("/WEB-INF/jsp/user_detail.jsp");
          dispatcher.forward(request, response);
        }
        request.setAttribute("user", user);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
        dispatcher.forward(request, response);
      }
    } catch (NullPointerException e) {
      response.sendRedirect("LoginServlet");
    }

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    request.setCharacterEncoding("UTF-8");
    HttpSession session = request.getSession();
    UserDao userDao = new UserDao();
    PasswordEncorder pe = new PasswordEncorder();
    String id = request.getParameter("id");

    int uId = Integer.valueOf(id);
    String password = request.getParameter("password");
    String confirmPassword = request.getParameter("password-confirm");
    String name = request.getParameter("name");
    User user = new User();
    String encordPassword;
    if (!(password.equals(confirmPassword)) || confirmPassword.equals(null) || name.equals(null)) {
      request.setAttribute("errMsg", "入力された内容が正しくありません");

      user.setId(uId);
      user.setPassword(password);
      user.setName(name);

      request.setAttribute("user", user);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
      dispatcher.forward(request, response);
    } else if (!(password.equals(""))) {
      encordPassword = pe.encordPassword(password);
      userDao.update(uId, name, encordPassword);
      User user1 = userDao.findById(id);
      request.setAttribute("user", user1);
      session.setAttribute("userInfo", user1);// sessionのuserInfoを上書きした。
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_detail.jsp");
      dispatcher.forward(request, response);
    } else if (password.equals("")) {
      userDao.update(uId, name);
      User user1 = userDao.findById(id);
      request.setAttribute("user", user1);
      session.setAttribute("userInfo", user1);// sessionのuserInfoを上書きした。
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_detail.jsp");
      dispatcher.forward(request, response);
    }

  }
}
