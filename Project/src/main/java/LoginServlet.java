

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public LoginServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    request.setCharacterEncoding("UTF-8");

    String loginId = request.getParameter("loginId");
    String password = request.getParameter("password");

    try {
      if (!loginId.equals(null) || !password.equals(null)) {
        // user情報を取ってきてnullならログインできない
        UserDao userDao = new UserDao();
        PasswordEncorder passwordEncorder = new PasswordEncorder();
        String encordPassword = passwordEncorder.encordPassword(password);
        User user = userDao.findByLoginInfo(loginId, encordPassword);
        if (user.equals(null)) {
          request.setAttribute("errMsg", "ログインまたはパスワードが異なります。");
          request.setAttribute("loginId", loginId);

          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
          dispatcher.forward(request, response);
          return;
        }

        HttpSession session = request.getSession();
        session.setAttribute("userInfo", user);
        // 掲示板名をsessionに追加
        session.setAttribute("boardTitle", "国内スポット");
        response.sendRedirect("ThreadListServlet");
        // threadeListServlet にリダイレクト
      }

    } catch (NullPointerException e) {
      request.setAttribute("loginId", loginId);
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
      dispatcher.forward(request, response);
      return;
    }



  }

}
