

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Dao.PrefectureDao;
import Dao.SituationDao;
import Dao.ThreadDao;
import model.Prefecture;
import model.Situation;
import model.Thread;
import model.User;

/**
 * Servlet implementation class ThreadListServlet
 */
@WebServlet("/ThreadListServlet")
public class ThreadListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ThreadListServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    HttpSession session = request.getSession();
    // String userId = (String) request.getAttribute("userId");
    User user = (User) session.getAttribute("userInfo");
    PrefectureDao prefectureDao = new PrefectureDao();
    SituationDao situationDao = new SituationDao();
    ThreadDao threadDao = new ThreadDao();
    try {
      if (!(user.equals(null))) {
        List<Prefecture> prefectureList = prefectureDao.findAll();
        List<Situation> situationList = situationDao.findAll();
        // threadの情報をすべて取ってくる
        List<Thread> threadList = threadDao.findAll();
        request.setAttribute("user", user);
        request.setAttribute("prefectureList", prefectureList);
        request.setAttribute("situationList", situationList);
        request.setAttribute("threadList", threadList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/thread_list.jsp");
        dispatcher.forward(request, response);
      }
    } catch (NullPointerException e) {
      response.sendRedirect("LoginServlet");
    }

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    HttpSession session = request.getSession();
    request.setCharacterEncoding("UTF-8");

    User user = (User) session.getAttribute("userInfo");
    String title = request.getParameter("title");
    String prefecture = request.getParameter("prefectureId");
    String situation = request.getParameter("situation-id");
    String startDate = request.getParameter("start-date");
    String endDate = request.getParameter("end-date");

    ThreadDao threadDao = new ThreadDao();
    PrefectureDao prefectureDao = new PrefectureDao();
    SituationDao situationDao = new SituationDao();
    List<Thread> threadList = threadDao.search(title, prefecture, situation, startDate, endDate);


    List<Prefecture> prefectureList = prefectureDao.findAll();
    List<Situation> situationList = situationDao.findAll();
    request.setAttribute("user", user);
    request.setAttribute("title", title);
    request.setAttribute("prefecture", prefecture);
    request.setAttribute("situation", situation);
    request.setAttribute("startDate", startDate);
    request.setAttribute("endDate", endDate);
    request.setAttribute("prefectureList", prefectureList);
    request.setAttribute("situationList", situationList);
    request.setAttribute("threadList", threadList);
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/thread_list.jsp");
    dispatcher.forward(request, response);



  }

}
