package model;

import java.io.Serializable;
import java.sql.Timestamp;

public class Comment implements Serializable {
  private int id;
  private int thread_id;
  private int user_id;
  private String comment;
  private Timestamp create_date;


  public Comment() {

  }


  public Comment(int id, int threadId, int userId, String comment, Timestamp createDate) {
    this.id = id;
    this.thread_id = threadId;
    this.user_id = userId;
    this.comment = comment;
    this.create_date = createDate;
  }


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getThread_id() {
    return thread_id;
  }

  public void setThread_id(int thread_id) {
    this.thread_id = thread_id;
  }

  public int getUser_id() {
    return user_id;
  }

  public void setUser_id(int user_id) {
    this.user_id = user_id;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Timestamp getCreate_date() {
    return create_date;
  }

  public void setCreated_date(Timestamp create_date) {
    this.create_date = create_date;
  }


}
