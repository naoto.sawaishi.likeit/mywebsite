package model;

import java.io.Serializable;
import java.sql.Timestamp;

public class User implements Serializable {
  private int id;
  private String login_id;
  private String name;
  private String password;
  private boolean is_admin;
  private Timestamp create_date;
  private Timestamp update_date;

  public User() {

  }

  public User(int id, String loginId, String name, String password, boolean isAdimn,
      Timestamp createDate, Timestamp updateDate) {
    this.id = id;
    this.login_id = loginId;
    this.name = name;
    this.password = password;
    this.is_admin = isAdimn;
    this.create_date = createDate;
    this.update_date = updateDate;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getLogin_id() {
    return login_id;
  }

  public void setLogin_id(String login_id) {
    this.login_id = login_id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isIs_admin() {
    return is_admin;
  }

  public void setIs_admin(boolean is_admin) {
    this.is_admin = is_admin;
  }

  public Timestamp getCreate_date() {
    return create_date;
  }

  public void setCreate_date(Timestamp create_date) {
    this.create_date = create_date;
  }

  public Timestamp getUpdate_date() {
    return update_date;
  }

  public void setUpdate_date(Timestamp update_date) {
    this.update_date = update_date;
  }


}
