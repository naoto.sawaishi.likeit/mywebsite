package model;

import java.io.Serializable;
import java.sql.Timestamp;

public class Thread implements Serializable {
  private int id;
  private String title;
  private String content;
  private int prefecture_id;
  private int situation_id;
  private int user_id;
  private Timestamp create_date;


  public Thread() {

  }

  public Thread(int id, String title, String content, int prefectureId, int situationId, int userId,
      Timestamp createDate) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.prefecture_id = prefectureId;
    this.situation_id = situationId;
    this.user_id = userId;
    this.create_date = createDate;
  }


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public int getPrefecture_id() {
    return prefecture_id;
  }

  public void setPrefecture_id(int prefecture_id) {
    this.prefecture_id = prefecture_id;
  }

  public int getSituation_id() {
    return situation_id;
  }

  public void setSituation_id(int situation_id) {
    this.situation_id = situation_id;
  }

  public int getUser_id() {
    return user_id;
  }

  public void setUser_id(int user_id) {
    this.user_id = user_id;
  }

  public Timestamp getCreate_date() {
    return create_date;
  }

  public void setCreate_date(Timestamp createDate) {
    this.create_date = createDate;
  }



}
