

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserAddServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_add.jsp");
        dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    request.setCharacterEncoding("UTF-8");

    UserDao userDao = new UserDao();
    PasswordEncorder passwordEncorder = new PasswordEncorder();
    String loginId = request.getParameter("login-id");
    String password = request.getParameter("password");
    String confirmPassword = request.getParameter("password-confirm");
    String name = request.getParameter("name");
    Boolean user = userDao.findByLoginId(loginId);

    if (!(password.equals(confirmPassword)) || password.equals("") || loginId.equals("")
        || name.equals("") || confirmPassword.equals("") || user) {
      // Date birthDate = Date.valueOf(birthday);
      request.setAttribute("errMsg", "入力された内容は正しくありません。");
      User user1 = new User();
      user1.setLogin_id(loginId);
      user1.setName(name);
      // user.setBirthday(birthDate);
      // request.setAttribute("loginId", loginId);
      // request.setAttribute("name", name);
      request.setAttribute("user", user1);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_add.jsp");
      dispatcher.forward(request, response);
    } else {
      String encordPassword = passwordEncorder.encordPassword(password);
      userDao.insert(loginId, name, encordPassword);

      response.sendRedirect("LoginServlet");
    }
  }
}
