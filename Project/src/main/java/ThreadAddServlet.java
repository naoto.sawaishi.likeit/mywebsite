

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Dao.PrefectureDao;
import Dao.SituationDao;
import Dao.ThreadDao;
import Dao.UserDao;
import model.Prefecture;
import model.Situation;
import model.User;

/**
 * Servlet implementation class ThreadAddServlet
 */
@WebServlet("/ThreadAddServlet")
public class ThreadAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ThreadAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("userInfo");
        String id = (String) request.getParameter("id");
        PrefectureDao prefectureDao = new PrefectureDao();
        SituationDao situationDao = new SituationDao();

        try {
          if (!user.equals(null)) {
            List<Prefecture> prefectureList = prefectureDao.findAll();

            List<Situation> situationList = situationDao.findAll();
            request.setAttribute("user", user);
            request.setAttribute("prefectureList", prefectureList);
            request.setAttribute("situationList", situationList);
            request.setAttribute("id", id);
            RequestDispatcher dispatcher =
                request.getRequestDispatcher("/WEB-INF/jsp/thread_add.jsp");
            dispatcher.forward(request, response);
          }
        } catch (NullPointerException e) {
          response.sendRedirect("LoginServlet");
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.setCharacterEncoding("UTF-8");

        String title = request.getParameter("title");
        String prefectureId = request.getParameter("prefecture");
        String situationId = request.getParameter("situation");
        String content = request.getParameter("body");
        String userId = request.getParameter("user-id");
        System.out.println(userId + "aaa");
        UserDao userDao = new UserDao();
        Boolean user = userDao.findId(userId);
        ThreadDao threadDao = new ThreadDao();
        PrefectureDao prefectureDao = new PrefectureDao();
        SituationDao situationDao = new SituationDao();
        List<Prefecture> prefectureList = prefectureDao.findAll();
        List<Situation> situationList = situationDao.findAll();

        if (title.equals("") || prefectureId.equals("選んでください") || situationId.equals("選んでください")
            || !(user)) {
          request.setAttribute("errMsg", "入力された内容が正しくありません");
          request.setAttribute("title", title);
          request.setAttribute("content", content);
          request.setAttribute("prefectureList", prefectureList);
          request.setAttribute("situationList", situationList);
          request.setAttribute("id", userId);

          RequestDispatcher dispatcher =
              request.getRequestDispatcher("/WEB-INF/jsp/thread_add.jsp");
          dispatcher.forward(request, response);
        } else {
          threadDao.insert(title, content, prefectureId, situationId, userId);
          // request.setAttribute("userId", userId);
          response.sendRedirect("ThreadListServlet");
        }


	}

}
