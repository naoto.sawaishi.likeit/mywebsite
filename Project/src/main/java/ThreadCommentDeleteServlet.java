

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Dao.ThreadDao;
import model.Thread;
import model.User;

/**
 * Servlet implementation class ThreadDeleteServlet
 */
@WebServlet("/ThreadCommentDeleteServlet")
public class ThreadCommentDeleteServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ThreadCommentDeleteServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");
    String threadId = request.getParameter("id");
    ThreadDao threadDao = new ThreadDao();
    Thread thread = threadDao.findById(threadId);

    try {
      if (!user.equals(null)) {
        request.setAttribute("user", user);
        request.setAttribute("thread", thread);

        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/thread_comment_delete.jsp");
        dispatcher.forward(request, response);
      }
    } catch (NullPointerException e) {
      response.sendRedirect("LoginServlet");
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    request.setCharacterEncoding("UTF-8");

    String threadId = request.getParameter("thread-id");
    ThreadDao threadDao = new ThreadDao();
    threadDao.delete(threadId);

    response.sendRedirect("ThreadListServlet");
  }

}
