

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Dao.CommentDao;
import Dao.ThreadDao;
import Dao.UserDao;
import model.Comment;
import model.Thread;
import model.User;

/**
 * Servlet implementation class CommentAddServlet
 */
@WebServlet("/CommentAddServlet")
public class CommentAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CommentAddServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");
    String threadId = request.getParameter("id");// 値は取れている
    // thread情報を取ってくるメソッドを作って呼ぶ threadIdで検索する
    UserDao userDao = new UserDao();
    ThreadDao threadDao = new ThreadDao();

    try {
      if (!(user.equals(null))) {
        List<User> users = userDao.findAll();
        Thread thread = threadDao.findById(threadId);
        request.setAttribute("thread", thread);
        CommentDao commentDao = new CommentDao();
        List<Comment> commentList = commentDao.findAll();
        request.setAttribute("user", user);
        request.setAttribute("users", users);

        request.setAttribute("commentList", commentList);

        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/thread_comment.jsp");
        dispatcher.forward(request, response);
      }
    } catch (NullPointerException e) {
      response.sendRedirect("LoginServlet");
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    HttpSession session = request.getSession();
    request.setCharacterEncoding("UTF-8");
    User user = (User) session.getAttribute("userInfo");
    int userId = user.getId();
    UserDao userDao = new UserDao();
    ThreadDao threadDao = new ThreadDao();

    String threadId = request.getParameter("thread-id");// 値を取れている
    String commentBody = request.getParameter("comment-body");
    CommentDao commentDao = new CommentDao();
    Thread thread = threadDao.findById(threadId);
    List<User> users = userDao.findAll();


    if (!(commentBody.equals(""))) {
      commentDao.insert(threadId, userId, commentBody);
      List<Comment> commentList = commentDao.findAll();
      request.setAttribute("user", user);
      request.setAttribute("users", users);
      request.setAttribute("thread", thread);
      request.setAttribute("commentList", commentList);
      RequestDispatcher dispatcher =
          request.getRequestDispatcher("/WEB-INF/jsp/thread_comment.jsp");
      dispatcher.forward(request, response);
    } else if (commentBody.equals("")) {
      request.setAttribute("errMsg", "内容が入力されていません");
      List<Comment> commentList = commentDao.findAll();
      request.setAttribute("user", user);
      request.setAttribute("users", users);
      request.setAttribute("thread", thread);
      request.setAttribute("commentList", commentList);

      RequestDispatcher dispatcher =
          request.getRequestDispatcher("/WEB-INF/jsp/thread_comment.jsp");
      dispatcher.forward(request, response);
    }



  }

}
