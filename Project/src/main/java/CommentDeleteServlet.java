

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Dao.CommentDao;
import Dao.ThreadDao;
import Dao.UserDao;
import model.Comment;
import model.Thread;
import model.User;

/**
 * Servlet implementation class CommentDeleteServlet
 */
@WebServlet("/CommentDeleteServlet")
public class CommentDeleteServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CommentDeleteServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    HttpSession session = request.getSession();
    request.setCharacterEncoding("UTF-8");
    User user = (User) session.getAttribute("userInfo");
    String commentId = request.getParameter("id");
    String threadId = request.getParameter("thread-id");

    try {
      if (!user.equals(null)) {
        CommentDao commentDao = new CommentDao();
        UserDao userDao = new UserDao();
        User threadUser = userDao.findById(threadId);
        ThreadDao threadDao = new ThreadDao();
        Thread thread = threadDao.findById(threadId);
        commentDao.delete(commentId);
        List<Comment> commentList = commentDao.findAll();

        request.setAttribute("user", user);
        request.setAttribute("threadUser", threadUser);
        request.setAttribute("thread", thread);
        request.setAttribute("commentList", commentList);

        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/thread_comment.jsp");
        dispatcher.forward(request, response);
      }
    } catch (NullPointerException e) {
      response.sendRedirect("LoginServlet");
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub

  }

}
