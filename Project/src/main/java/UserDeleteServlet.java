

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Dao.CommentDao;
import Dao.ThreadDao;
import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        HttpSession session = request.getSession();
        User LoginUser = (User) session.getAttribute("userInfo");

        try {
          if (!LoginUser.equals(null)) {
            UserDao userDao = new UserDao();
            String id = request.getParameter("id");
            User user = userDao.findById(id);

            request.setAttribute("user", user);

            RequestDispatcher dispatcher =
                request.getRequestDispatcher("/WEB-INF/jsp/user_delete.jsp");
            dispatcher.forward(request, response);
          }
        } catch (NullPointerException e) {
          response.sendRedirect("LoginServlet");
        }

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        // ユーザを削除する前に、削除するユーザのコメントとスレッドを削除してからユーザを削除する。
        
        UserDao userDao = new UserDao();
        ThreadDao threadDao = new ThreadDao();
        CommentDao commentDao = new CommentDao();
        String id = request.getParameter("user-id");
        System.out.println(id);
        commentDao.deleteByUserId(id);
        threadDao.deleteByUserId(id);
        userDao.delete(Integer.valueOf(id));

        response.sendRedirect("ThreadListServlet");
        
	}

}
