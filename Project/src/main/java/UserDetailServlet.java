

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserDetailServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    HttpSession session = request.getSession();
    String id = request.getParameter("id");
    UserDao userDao = new UserDao();
    User user = (User) session.getAttribute("userInfo");
    // user情報がない場合 ログイン画面へ遷移

    // 管理者ならユーザのリストを表示 まだページを作ってない。(未実装)
    // if (user.isIs_admin()) {
    // User userList = (User) userDao.findAll();
    // request.setAttribute("userList", userList);
    // RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_list.jsp");
    // dispatcher.forward(request, response);
    // return;
    // }
    try {
      if (!(user.equals(null))) {
        User user1 = userDao.findById(id);
        if (user1 == null) {
          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
          dispatcher.forward(request, response);
          return;
        }
        request.setAttribute("user", user1);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_detail.jsp");
        dispatcher.forward(request, response);
      }

    } catch (NullPointerException e) {
      response.sendRedirect("LoginServlet");
    }



  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    doGet(request, response);
  }

}
