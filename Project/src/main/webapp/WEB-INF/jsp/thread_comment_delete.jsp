<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${boardTitle } | スレッド削除</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- BootstrapのCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
    <!-- ヘッダー -->
    <header class="sticky-top mb-5">
      <nav class="navbar navbar-expand-lg navbar-light bg-white  pt-5">
          <div class="container-fluid">
            <div class="collapse navbar-collapse row justify-content-center" id="navbarTogglerDemo02">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0   border-bottom">
                <li class="nav-item">
                    <!-- Homeへ遷移 -->
                    <a class="nav-link" href="ThreadListServlet">Home</a>
                </li>
                <li class="nav-item ">
							<!-- ユーザ詳細画面へ遷移 -->
							<c:if test="${user.is_admin }">
								<a class="nav-link" href="UserListServlet">${userInfo.name }さん</a>
							</c:if>
							<c:if test="${!user.is_admin }">
								<a class="nav-link" href="UserDetailServlet?id=${userInfo.id }">${userInfo.name}さん</a>
							</c:if>
						</li>
                <li class="nav-item">
                  <!-- ログイン画面へ遷移 -->
                  <a class="nav-link" href="LogoutServlet">logout</a>
                </li>
              </ul>
            </div>
          </div>
      </nav>
  </header>

    <div class="container">
        <div class="row">
            <div class="col-6 offset-3">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">スレッドを削除します</h3>
                    </div>
                    <div class="card-body">
                        <p class="card-text text-danger text-center">
                            ${thread.title } を削除してよろしいですか？
                        </p>
                        <p class="card-text text-center">
                            このスレッドに関連する全てのコメントも同時に削除されます。
                        </p>

                        <div class="row">
                            <div class="col">
                                <!--スレッド表示・コメント投稿ページへ戻るリンク -->
                                <a href="ThreadListServlet" class="btn btn-outline-primary btn-block">戻る</a>
                            </div>
                            <div class="col">
                                <!-- スレッドとコメントを削除するフォーム -->
                                <form method="post" action="ThreadCommentDeleteServlet">
                                    <!-- 削除するThreadのIDをhiddenでもたせる -->
                                    <input type="hidden" name="thread-id" value="${thread.id}">
                                    <button type="submit" class="btn btn-danger btn-block">削除する</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</body>

</html>