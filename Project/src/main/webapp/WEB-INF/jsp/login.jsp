<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>${boardTitle } | ログイン</title>
	<!-- header.cssの読み込み -->
	<link href="css/header.css" rel="stylesheet" type="text/css" />
	<!-- Bootstrapの読み込み -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
		integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
			<ul class="navbar-nav flex-row">
				<li class="nav-item">
					<!-- ユーザ登録ページへのリンク -->
					<a class="nav-link" href="user_add.html">ユーザ登録</a>
				</li>
			</ul>
		</nav>
	</header>

	<div class="container">
		<div class="row">
			<div class="col">
				<h3 class="text-center">ログイン</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3">
				<!-- エラーメッセージ start -->
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">エラーメッセージ</div>
				</c:if>
				<!--エラーメッセージ  end  -->
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3">
				<!--   ログインフォーム   -->
				<form action="LoginServlet" method="post">

					<div class="form-group row">
						<label for="user-id" class="control-label col-3">ログインID</label>
						<div class="col-9">
							<!--ログインID入力 -->
							<input type="text" name="loginId" id="loginId" class="form-control" value="${loginId }">
						</div>
					</div>

					<div class="form-group row">
						<label for="password" class="control-label col-3">パスワード</label>
						<div class="col-9">
							<!--パスワード入力-->
							<input type="password" name="password" id="password" class="form-control">
						</div>
					</div>

					<div>
						<button type="submit" class="btn btn-primary btn-block form-submit">ログイン</button>
					</div>
				</form>
				<div class="text-xl-center mt-3">
					<a class="return-btn" href="UserAddServlet">新規登録はこちら</a>
				  </div>
			</div>
		</div>
	</div>

</body>

</html>