<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>${boardTitle } | ユーザ登録</title>
	<!-- header.cssの読み込み -->
	<link href="css/header.css" rel="stylesheet" type="text/css" />
	<!-- Bootstrapの読み込み -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
		integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<!-- <header class="sticky-top mb-5 pt-4">
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <div class="container-fluid row justify-content-center">
				<a class="navbar-brand border-bottom" href="#">login</a>
				<!-- <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button> 
            </div>
        </nav>
    </header> -->


	<div class="container mt-5">
		<div class="row">
			<div class="col-6 offset-3 text-center mb-3">
				<h2>ユーザ新規登録</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-6 offset-3">
				<!-- エラーメッセージ start -->
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg }</div>
				</c:if>
				<!--エラーメッセージ  end  -->
			</div>
		</div>
		<div class="row">
			<div class="col-6 offset-3">
				<!--   ユーザー登録フォーム   -->
				<form method="post" action="UserAddServlet">

					<div class="form-group row">
						<label for="user-id" class="control-label col-3">ログインID</label>
						<div class="col-9">
							<!--   ログインID（無効化）   -->
							<input type="text" name="login-id" id="loginId" class="form-control"
								value="${user.login_id }">
						</div>
					</div>
					<div class="form-group row">
						<label for="password" class="control-label col-3">パスワード</label>
						<div class="col-9">
							<!--   パスワード   -->
							<input type="password" name="password" id="password" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label for="password-confirm" class="control-label col-3">パスワード<br>(確認)</label>
						<div class="col-9">
							<!--   パスワード（確認用）   -->
							<input type="password" name="password-confirm" id="password-confirm" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label for="user-name" class="control-label col-3">ユーザ名</label>
						<div class="col-9">
							<!--   ユーザー名   -->
							<input type="text" name="name" id="user-name" class="form-control" value="${user.name }">
						</div>
					</div>

					<div>
						<button type="submit" class="btn btn-primary btn-block form-submit mt-3">登録</button>
					</div>
				</form>
				<div class="row mt-3">
					<div class="col text-xl-center">
						<!-- 戻るボタン login画面へ遷移-->
						<a href="LoginServlet" class="text-primary">戻る</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>