<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${boardTitle } | スレッド詳細</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- BootstrapのCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
    <!-- ヘッダー -->
    <header class="sticky-top mb-5">
      <nav class="navbar navbar-expand-lg navbar-light bg-white  pt-5">
          <div class="container-fluid">
            <div class="collapse navbar-collapse row justify-content-center" id="navbarTogglerDemo02">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0   border-bottom">
                <li class="nav-item">
                    <!-- Homeへ遷移 -->
                    <a class="nav-link" href="ThreadListServlet">Home</a>
                </li>
                <li class="nav-item ">
							<!-- ユーザ詳細画面へ遷移 -->
							<c:if test="${user.is_admin }">
								<a class="nav-link" href="UserListServlet">${userInfo.name }さん</a>
							</c:if>
							<c:if test="${!user.is_admin }">
								<a class="nav-link" href="UserDetailServlet?id=${userInfo.id }">${userInfo.name}さん</a>
							</c:if>
						</li>
                <li class="nav-item">
                  <!-- ログイン画面へ遷移 -->
                  <a class="nav-link" href="LogoutServlet">logout</a>
                </li>
              </ul>
            </div>
          </div>
      </nav>
  </header>

    <div class="container pb-4">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <!--スレッドタイトルを入れる  スレッド情報をを使う-->
                        <h3 class="text-center">${thread.title }</h3>
                    </div>
                    <div class="card-body">
                        <p class="card-text">
                            <!--スレッド本文-->
                            ${thread.content }
                        </p>
                        
                        <div class="d-flex justify-content-between">
                            <!--スレッド投稿者、時間-->
                            <c:forEach var="threadUser" items="${users }">
	                            <c:if test="${thread.user_id == threadUser.id }">
	                            	<p class="card-text text-muted">${threadUser.name } さん</p><!-- 名前を特定するメソッドを作る -->
                            		<p class="card-text text-muted"><fmt:formatDate value="${thread.create_date }" pattern="yyyy年MM月dd日 HH:mm" /></p>
	                            </c:if>
                            </c:forEach>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-10 offset-2">
                <!-- コメント一覧表示用 fo文 おそらく INNER JOIN を使う -->
                <c:forEach var="commentUser" items="${commentList}">
                	<c:if test="${commentUser.thread_id == thread.id }">
	                    <div class="card">
	                        <div class="card-body">
	                            <p class="card-text">
	                                <!--コメント本文-->
	                                ${commentUser.comment }
	                            </p>
	                            <!-- 画像が貼れてもいい -->
	                            <!--コメント削除ボタン表示の制御-->
	                            <c:if test="${userInfo.id == commentUser.user_id}">
	                                <div class="text-right">
	                                    <!--コメント削除ページへのリンク-->
	                                    <a type="button" class="btn btn-danger"
	                                        href="CommentDeleteServlet?id=${commentUser.id}&thread-id=${commentUser.thread_id}">削除する</a>
	                                </div>
	                            </c:if>
	                            <div class="d-flex justify-content-between">
	                                <!--コメント投稿者、時間-->
	                                <c:forEach var="user" items="${users }">
	                                	<c:if test="${commentUser.user_id == user.id }">
	                                		<p class="card-text text-muted">${user.name }</p>
	                                	</c:if>
	                                </c:forEach>
	                                
	                                
	                                <p class="card-text text-muted"><fmt:formatDate value="${commentUser.create_date }" pattern="yyyy年MM月dd日 HH:mm" /></p>
	                            </div>
	                        </div>
	                    </div>
                    </c:if>
                </c:forEach>
            </div>
        </div>

        <!-- コメント投稿エリア -->
        <div class="row mt-3">
            <div class="col">

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <!-- コメント投稿時のエラーメッセージ start -->
                                <c:if test="${errMsg != null}">
                                    <div class="alert alert-danger" role="alert">${errMsg }</div>
                                </c:if>
                                <!-- コメント投稿時のエラーメッセージ  end  -->
                            </div>
                        </div>
                        <!-- コメント投稿フォーム start -->
                        <form method="post" action="CommentAddServlet">
                            <!-- コメント投稿に必要なスレッドIDをhiddenでもたせる -->
                            <input type="hidden" name="thread-id" value="${thread.id}">
                            <div class="form-group row">
                                <div class="col-10">
                                    <!-- コメント本文 入力 -->
                                    <textarea class="form-control" id="comment-body" name="comment-body"
                                        rows="3"></textarea>
                                </div>
                                <div class="col-2">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">コメント<br>投稿</button>
                                </div>
                            </div>
                        </form>
                        <!-- コメント投稿フォーム end -->
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col text-center">
                <!-- スレッド一覧に戻るリンク -->
                <a href="ThreadListServlet" class="btn btn-outline-primary px-5">戻る</a>
            </div>
        </div>
    </div>
</body>

</html>