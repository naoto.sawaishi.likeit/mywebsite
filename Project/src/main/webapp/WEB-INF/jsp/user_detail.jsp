<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>ユーザ詳細画面</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous" />
<!-- home.cssの読み込み -->
<link href="css/home.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<header class="sticky-top mb-5">
		<nav class="navbar navbar-expand-lg navbar-light bg-white  pt-5">
			<div class="container-fluid">
				<div class="collapse navbar-collapse row justify-content-center"
					id="navbarTogglerDemo02">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0   border-bottom">
						<li class="nav-item">
							<!-- Homeへ遷移 --> <a class="nav-link" href="ThreadListServlet">Home</a>
						</li>
						<li class="nav-item ">
							<!-- ユーザ詳細画面へ遷移 --> <c:if test="${user.is_admin }">
								<a class="nav-link" href="UserListServlet">${userInfo.name }さん</a>
							</c:if> 
							<c:if test="${!user.is_admin }">
								<a class="nav-link" href="UserDetailServlet?id=${userInfo.id }">${userInfo.name}さん</a>
							</c:if>
						</li>
						<li class="nav-item">
							<!-- ログイン画面へ遷移 --> <a class="nav-link" href="LogoutServlet">logout</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->
	<div class="container-fluid">
		<div class="row mb-4">
			<div class="col">
				<h1 class="text-center">ユーザ情報詳細参照</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3 text-center">
				<div class="row">
					<label for="loginId" class="col-5 font-weight-bold">ログインID</label>
					<div class="col">
						<p>${user.login_id }</p>
					</div>
				</div>

				<div class="row">
					<label for="userName" class="col-5 font-weight-bold">ユーザ名</label>
					<div class="col">
						<p>${user.name }</p>
					</div>
				</div>

				<div class="row">
					<label for="createDate" class="col-5 font-weight-bold">新規登録日</label>
					<div class="col">
						<p>
							<fmt:formatDate value="${user.create_date }"
								pattern="yyyy年MM月dd日" />
						</p>
					</div>
				</div>

				<div class="row">
					<label for="updateDate" class="col-5 font-weight-bold">更新日</label>
					<div class="col">
						<p>
							<fmt:formatDate value="${user.update_date }"
								pattern="yyyy年MM月dd日" />
						</p>
					</div>
				</div>
				<div class="row mt-2">
					<a type="button" class="btn btn-outline-primary col mr-1"
						href="UserUpdateServlet?id=${user.id }">更新</a> <a type="button"
						class="btn btn-outline-danger col"
						href="UserDeleteServlet?id=${user.id}">削除</a>
				</div>
				<!-- login画面へ遷移 -->
				<div class="text-xl-center mt-3">
					<a class="return-btn" href="ThreadListServlet">戻る</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
