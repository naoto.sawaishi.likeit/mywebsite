<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>${boardTitle } | スレッド投稿</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body class="pt-40">
    <!-- ヘッダー -->
   <header class="sticky-top mb-5">
      <nav class="navbar navbar-expand-lg navbar-light bg-white  pt-5">
          <div class="container-fluid">
            <div class="collapse navbar-collapse row justify-content-center" id="navbarTogglerDemo02">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0   border-bottom">
                <li class="nav-item">
                    <!-- Homeへ遷移 -->
                    <a class="nav-link" href="ThreadListServlet">Home</a>
                </li>
                <li class="nav-item ">
							<!-- ユーザ詳細画面へ遷移 -->
							<c:if test="${user.is_admin }">
								<a class="nav-link" href="UserListServlet">${userInfo.name }さん</a>
							</c:if>
							<c:if test="${!user.is_admin }">
								<a class="nav-link" href="UserDetailServlet?id=${userInfo.id }">${userInfo.name}さん</a>
							</c:if>
						</li>
                <li class="nav-item">
                  <!-- ログイン画面へ遷移 -->
                  <a class="nav-link" href="LogoutServlet">logout</a>
                </li>
              </ul>
            </div>
          </div>
      </nav>
  </header>


    <div class="container mt-6">
        <div class="row ">
            <!-- classからcol-6を削除 -->
            <div class=" mx-auto pb-2">
                <h2>スレッド追加</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-6 offset-3">
                <!-- エラーメッセージ start -->
                <c:if test="${errMsg != null}">
                    <div class="alert alert-danger" role="alert">${errMsg }</div>
                </c:if>
                <!--エラーメッセージ  end  -->
            </div>
        </div>

        <div class="row">
            <div class="col-6 offset-3">
                <!--   スレッド投稿フォーム   -->
                <form method="post" action="ThreadAddServlet">
                    <input type="hidden" name="user-id" value="${id}">
                    <div class="form-group row">
                        <label for="title" class="control-label col-2">タイトル</label>
                        <div class="col-10">
                            <!-- スレッドのタイトル 入力 -->
                            <input type="text" name="title" class="form-control" value="${title }">
                        </div>
                    </div>
                    <!-- 追加 -->
                    <div class="form-group row">
                        <label for="title" class="control-label col-2">都道府県</label>
                        <div class="col-4">
                        	<select class="form-select form-control" name="prefecture" size="1">
                        	<!-- optionをforEachで繰り返す -->
                        	<option hidden>選んでください</option>
                        		<c:forEach var="prefecture" items="${prefectureList}">
	                            	<option value="${prefecture.id }">${prefecture.name}</option>
	                            </c:forEach>
                            </select>
                        </div>
                    </div>
                    <!-- 追加 -->
                    <div class="form-group row">
                        <label for="title" class="control-label col-2">キーワード</label>
                        <div class="col-4">
                            <select class="form-select form-control"  name="situation" size="1">
                            	  <option hidden>選んでください</option>
                            	  <c:forEach var="situation" items="${situationList}">
	                                    <option value="${situation.id }">${situation.name}</option>
	                              </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="body" class="control-label col-2">本文</label>
                        <div class="col-10">
                            <!-- スレッドの本文 入力 -->
                            <textarea class="form-control" id="body" name="body" rows="4" value="">${content }</textarea>
                        </div>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary btn-block form-submit">登録</button>
                    </div>
                </form>
                <div class="row mt-3 text-xl-center">
                    <div class="col">
                        <!-- スレッド一覧ページに戻るリンク -->
                        <a href="ThreadListServlet" class="text-primary">戻る</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>