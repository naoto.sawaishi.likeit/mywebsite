<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>${boardTitle } | ホーム</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- home.cssの読み込み -->
<link href="css/home.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<!-- ヘッダー -->
	<header class="sticky-top mb-5">
		<nav class="navbar navbar-expand-lg navbar-light bg-white  pt-5">
			<div class="container-fluid">
				<div class="collapse navbar-collapse row justify-content-center"
					id="navbarTogglerDemo02">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0   border-bottom">
						<li class="nav-item">
							<!-- Homeへ遷移 --> <a class="nav-link" href="ThreadListServlet">Home</a>
						</li>
						<li class="nav-item ">
							<!-- ユーザ詳細画面へ遷移 -->
							<c:if test="${user.is_admin }">
								<a class="nav-link" href="UserListServlet">${userInfo.name }さん</a>
							</c:if>
							<c:if test="${!user.is_admin }">
								<a class="nav-link" href="UserDetailServlet?id=${userInfo.id }">${userInfo.name}さん</a>
							</c:if>
						</li>
						<li class="nav-item">
							<!-- ログイン画面へ遷移 --> <a class="nav-link" href="LogoutServlet">logout</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<div class="container pb-5">
		<div class="row">
			<div class="col">
				<h1 class="text-center">${boardTitle }</h1>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-body">
						<!-- 検索フォーム -->
						<form method="post" action="ThreadListServlet">
							<div class="form-group row">
								<label for="title" class="col-2 col-form-label">スレッドタイトル</label>
								<div class="col-10">
									<!-- タイトル入力 -->
									<input type="text" name="title" class="form-control" id="title"
										value="${title }">
								</div>
							</div>
							<!-- ここに追加していく カテゴリーを2つ追加 forEachで選択肢を表示する-->
							<input type="hidden" name="prefecture-id"
								value="${prefecture}">
							<div class="form-group row">
								<label for="prefecture" class="col-2 col-form-label">都道府県</label>
								<div class="col-3">
									<select class="form-select form-control" name="prefectureId"
										size="1">
										<option hidden>選択してください</option>
										<c:forEach var="prefecture" items="${prefectureList}">
											<option value="${prefecture.id }">${prefecture.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<!-- 追加 -->
							<input type="hidden" name="thread-id" value="${situation}">
							<div class="form-group row">
								<label for="situation" class="col-2 col-form-label">キーワード</label>
								<div class="col-3">
									<select class="form-select form-control" name="situation-id"
										size="1">
										<option hidden>選択してください</option>
										<c:forEach var="situation" items="${situationList}">
											<option value="${situation.id }">${situation.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-2 col-form-label">投稿日</label>
								<div class="col-10">
									<div class="row">
										<div class="col-5">
											<!-- 投稿日 （開始日） -->
											<input type="date" name="start-date" id="startPostDate"
												class="form-control" value="${startDate }">
										</div>
										<div class="col-2 text-center">~</div>
										<div class="col-5">
											<!-- 投稿日 （終了日） -->
											<input type="date" name="end-date" id="endDate"
												class="form-control" value="${endDate }">
										</div>
									</div>
								</div>
							</div>

							<div class="text-right">
								<button type="submit" class="btn btn-primary btn-lg form-submit">検索</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col mt-3 d-flex justify-content-between">
				<h3 class="text-center">スレッド一覧</h3>
				<!-- スレッド追加リンク-->
				<p>
					<a class="text-primary" href="ThreadAddServlet?id=${user.id }">スレッドを立てる</a>
				</p>
				<!-- スレッド作成にとべない -->
			</div>
		</div>

		<div class="row">
			<div class="col">
				<div class="card">
					<table class="table">
						<thead class="thead-light">
							<tr>
								<th>タイトル</th>
								<th>作成日</th>
								<th></th>
							</tr>
						</thead>
						<tbody><!-- if分入れる 管理者なら削除をすべて表示 -->
							<!-- スレッド表示用 for文 -->
							<c:forEach var="thread" items="${threadList}">
								<tr>
									<td class="col-9">
										<!-- スレッド表示・コメント投稿ページ リンク --> <a
										href="CommentAddServlet?id=${thread.id}" class="text-primary">${thread.title }</a>
									</td>
									<!-- スレッド表示・コメント投稿ページ リンク -->
									<td><fmt:formatDate value="${thread.create_date }" pattern="yyyy/MM/dd HH : mm" /></td>
									<td class="text-right col-1">
										<!-- スレッド削除ボタン 表示制御 --> <c:if
											test="${userInfo.id == thread.user_id || user.is_admin}">
											<!-- スレッド削除ボタン -->
											<a type="button" class="btn btn-danger"
												href="ThreadCommentDeleteServlet?id=${thread.id}">削除</a>
										</c:if>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</body>

</html>