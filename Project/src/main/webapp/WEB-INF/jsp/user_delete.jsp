<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ削除画面</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- home.cssの読み込み -->
<link href="css/home.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<header class="sticky-top mb-5">
		<nav class="navbar navbar-expand-lg navbar-light bg-white  pt-5">
			<div class="container-fluid">
				<div class="collapse navbar-collapse row justify-content-center"
					id="navbarTogglerDemo02">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0   border-bottom">
						<li class="nav-item">
							<!-- Homeへ遷移 --> <a class="nav-link" href="ThreadListServlet">Home</a>
						</li>
						<li class="nav-item ">
							<!-- ユーザ詳細画面へ遷移 --> <c:if test="${user.is_admin }">
								<a class="nav-link" href="UserListServlet">${userInfo.name }さん</a>
							</c:if> <c:if test="${!user.is_admin }">
								<a class="nav-link" href="UserDetailServlet?id=${userInfo.id }">${userInfo.name}さん</a>
							</c:if>
						</li>
						<li class="nav-item">
							<!-- ログイン画面へ遷移 --> <a class="nav-link" href="LogoutServlet">logout</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3 mb-5">
				<div class="row">
					<div class="col text-center">
						<h1 class="text-center mb-4">ユーザ削除確認</h1>

						<p>ログインID: ${user.login_id }</p>
						<p>を本当に削除してよろしいでしょうか？</p>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<a href="UserDetailServlet?id=${user.id }"
								class="btn btn-light btn-block">いいえ</a>
					</div>
					<div class="col">
						<form action="UserDeleteServlet" method="post">
							<input type="hidden" name="user-id" value="${user.id}">
							<button type="submit" class="btn btn-primary btn-block">
								はい</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>