<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>${boardTitle }| ホーム</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- home.cssの読み込み -->
<link href="css/home.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<!-- ヘッダー -->
	<header class="sticky-top mb-5">
		<nav class="navbar navbar-expand-lg navbar-light bg-white  pt-5">
			<div class="container-fluid">
				<div class="collapse navbar-collapse row justify-content-center"
					id="navbarTogglerDemo02">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0   border-bottom">
						<li class="nav-item">
							<!-- Homeへ遷移 --> <a class="nav-link" href="ThreadListServlet">Home</a>
						</li>
						<li class="nav-item ">
							<!-- ユーザ詳細画面へ遷移 --> <c:if test="${user.is_admin }">
								<a class="nav-link" href="UserListServlet">${userInfo.name }さん</a>
							</c:if> <c:if test="${!user.is_admin }">
								<a class="nav-link" href="UserDetailServlet?id=${userInfo.id }">${userInfo.name}さん</a>
							</c:if>
						</li>
						<li class="nav-item">
							<!-- ログイン画面へ遷移 --> <a class="nav-link" href="LogoutServlet">logout</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<div class="container pb-5">
		<div class="row">
			<div class="col">
				<h1 class="text-center">ユーザ一覧</h1>
			</div>
		</div>


		<div class="row">
			<div class="col">
				<div class="card">
					<table class="table">
						<thead class="thead-light">
							<tr>
								<th>ユーザ名</th>
								<th>作成日</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<!-- if分入れる 管理者なら削除をすべて表示 -->
							<!-- スレッド表示用 for文 -->
							<c:forEach var="user" items="${userList}">
								<tr>
									<td class="col-9">
										<!-- スレッド表示・コメント投稿ページ リンク --> <a
										href="UserDetailServlet?id=${user.id}" class="text-primary">${user.name }</a>
									</td>
									<!-- スレッド表示・コメント投稿ページ リンク -->
									<td><fmt:formatDate value="${user.create_date }"
											pattern="yyyy/MM/dd HH : mm" /></td>
									<td class="text-right col-1">
										<!-- スレッド削除ボタン 表示制御 --> <!-- スレッド削除ボタン --> <a type="button"
										class="btn btn-danger"
										href="UserDeleteServlet?id=${user.id}">削除</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</body>

</html>