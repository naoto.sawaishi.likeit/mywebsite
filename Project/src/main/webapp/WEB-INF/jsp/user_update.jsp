<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>${boardTitle }| ユーザ情報更新</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header class="sticky-top mb-5">
		<nav class="navbar navbar-expand-lg navbar-light bg-white  pt-5">
			<div class="container-fluid">
				<div class="collapse navbar-collapse row justify-content-center"
					id="navbarTogglerDemo02">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0   border-bottom">
						<li class="nav-item">
							<!-- Homeへ遷移 --> <a class="nav-link" href="ThreadListServlet">Home</a>
						</li>
						<li class="nav-item ">
							<!-- ユーザ詳細画面へ遷移 --> <c:if test="${user.is_admin }">
								<a class="nav-link" href="UserListServlet">${userInfo.name }さん</a>
							</c:if> <c:if test="${!user.is_admin }">
								<a class="nav-link" href="UserDetailServlet?id=${userInfo.id }">${userInfo.name}さん</a>
							</c:if>
						</li>
						<li class="nav-item">
							<!-- ログイン画面へ遷移 --> <a class="nav-link" href="LogoutServlet">logout</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>


	<div class="container mt-5">
		<div class="row">
			<div class="col-6 offset-3 text-center mb-4">
				<h2>ユーザ情報 更新</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3">
				<!-- エラーメッセージ start -->
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">エラーメッセージ</div>
				</c:if>
				<!--エラーメッセージ  end  -->
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3">
				<!--   ユーザー情報更新フォーム   -->
				<form method="post" action="UserUpdateServlet">
					<div class="form_group row">
						<input type="hidden" name="id" value="${user.id }">
					</div>
					<div class="form-group row">
						<label for="user-id" class="control-label col-3">ログインID</label>
						<div class="col-9">
							<!--   ログインID（無効化）   -->
							<input type="text" name="login-id" id="loginId"
								class="form-control" value="${user.login_id }"
								disabled="disabled">
						</div>
					</div>
					<div class="form-group row">
						<label for="password" class="control-label col-3">パスワード</label>
						<div class="col-9">
							<!--   パスワード   -->
							<input type="password" name="password" id="password"
								class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label for="password-confirm" class="control-label col-3">パスワード<br>(確認)
						</label>
						<div class="col-9">
							<!--   パスワード（確認用）   -->
							<input type="password" name="password-confirm"
								id="password-confirm" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label for="user-name" class="control-label col-3">ユーザ名</label>
						<div class="col-9">
							<!--   ユーザー名   -->
							<input type="text" name="name" id="user-name"
								class="form-control" value="${user.name }">
						</div>
					</div>

					<div>
						<button type="submit"
							class="btn btn-primary btn-block form-submit">更新</button>
					</div>
				</form>
				<div class="row mt-3 text-lg-center">
					<div class="col">
						<!-- 戻るボタン-->
						<a href="UserDetailServlet?id=${user.id }" class="text-primary">戻る</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>